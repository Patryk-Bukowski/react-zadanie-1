import './App.css';
import FoodList from './components/food-list';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <FoodList />
      </header>
    </div>
  );
}

export default App;
