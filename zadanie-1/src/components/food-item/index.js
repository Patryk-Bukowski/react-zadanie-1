import React from 'react';
import FoodList from '../food-list';

const FoodItem = ({name}) => {
    return (
        <li>
            {name}
        </li>
    );
}
// class FoodItem extends React.Component{
//     render(){
//         return (
//             <li>
//             {this.props.name}
//         </li>
//         );
//     }
// }
export default FoodItem;