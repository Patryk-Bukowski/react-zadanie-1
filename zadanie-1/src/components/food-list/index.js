import React from 'react';
import FoodItem from '../food-item';

const FoodList = () => {
    return (
        <ul>
            <FoodItem name='Żurek' />
            <FoodItem name='Pomidorowa' />
            <FoodItem name='Rosół' />
            <FoodItem name='Jarzynowa' />
            <FoodItem name='Zupka Vifon' />
        </ul>
    );
}
// class FoodList extends React.Component {
//     render() {
//         return (
//             <ul>
//                 <FoodItem name='Żurek' />
//                 <FoodItem name='Pomidorowa' />
//                 <FoodItem name='Rosół' />
//                 <FoodItem name='Jarzynowa' />
//                 <FoodItem name='Zupka Vifon' />
//             </ul>
//         );
//     }
// }
export default FoodList;